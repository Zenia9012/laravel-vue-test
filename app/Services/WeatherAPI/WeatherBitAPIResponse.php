<?php


namespace App\Services\WeatherAPI;


use Psr\Http\Message\ResponseInterface;

class WeatherBitAPIResponse {

    /**
     * @var int
     */
    public $status;
    /**
     * @var \Psr\Http\Message\StreamInterface
     */
    public $body;
    /**
     * @var \string[][]
     */
    public $headers;

    /**
     * WeatherBitAPIResponse constructor.
     *
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->status = $response->getStatusCode();
        $this->body = $response->getBody();
        $this->headers = $response->getHeaders();
    }

    /**
     * return array forecast
     *
     * @return mixed
     */
    public function getForecast()
    {
        return json_decode($this->body->getContents(), true);
    }
}
