<?php


namespace App\Services\WeatherAPI;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

abstract class WeatherBitAPIRequest {

    /**
     * @var string
     */
    protected $key;
    /**
     * @var string
     */
    protected $url;
    /**
     * @var Client
     */
    private $client;

    /**
     * WeatherBitAPIRequest constructor.
     */
    public function __construct()
    {
        $this->key = config('services.weatherbit.key');
        $this->url = config('services.weatherbit.url');
        $this->client = new Client();
    }

    /**
     * @return WeatherBitAPIResponse
     * @throws GuzzleException
     */
    public function sendGet()
    {
        $response = $this->client->request('GET', $this->buildUrl());

        return new WeatherBitAPIResponse($response);
    }

    /**
     * @return mixed
     */
    abstract protected function buildUrl();
}
