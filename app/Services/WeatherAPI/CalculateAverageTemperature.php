<?php


namespace App\Services\WeatherAPI;


class CalculateAverageTemperature {

    /**
     * @var int
     */
    private $perDays = 10;

    /**
     * @var array
     */
    private $forecast;

    /**
     * CalculateAverageTemperature constructor.
     *
     * @param array $forecast
     * @throws \Exception
     */
    public function __construct(array $forecast)
    {
        if (isset($forecast['data'])) {
            $this->forecast = $forecast['data'];
        } else {
            throw new \Exception('Wrong format of forecast');
        }

    }

    /**
     * calculate average temperature
     *
     * @return float|int
     */
    private function calculateAverageTemperature()
    {
        $temperatureSum = 0;
        for ($i = 0; $i < $this->perDays; $i++) {
            $temperatureSum += $this->forecast[$i]['temp'];
        }

        return $temperatureSum / $this->perDays;
    }

    /**
     * return the average temperature
     *
     * @param int $perDays
     * @return float|int
     * @throws \Exception
     */
    public function getAverageTemperature($perDays = 10)
    {
        $this->perDays = $perDays;
        if ($this->perDays > count($this->forecast)) {
            throw new \Exception('Per days set more than days in forecast');
        }

        return $this->calculateAverageTemperature();
    }
}
