<?php


namespace App\Services\WeatherAPI;


use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

class WeatherCache {

    /**
     * put to cache
     *
     * @param     $key
     * @param     $value
     * @param int $time
     */
    public static function put($key, $value, $time = 120)
    {
        Cache::put(Str::lower($key), $value, $time);
    }

    /**
     * check if has cache specific key
     *
     * @param $key
     * @return bool
     */
    public static function has($key)
    {
        return Cache::has(Str::lower($key));
    }

    /**
     * return from cache specific value
     *
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        return Cache::get(Str::lower($key));
    }
}
