<?php


namespace App\Services\WeatherAPI;


use Exception;
use GuzzleHttp\Exception\GuzzleException;

class WeatherDailyRequest extends WeatherBitAPIRequest {

    /**
     * @var string
     */
    private $city;
    /**
     * @var string
     */
    private $countryCode;

    private $urlSuffix = 'forecast/daily?';

    /**
     * WeatherCurrentAPI constructor.
     *
     * @param string $city
     * @param string $countryCode
     */
    public function __construct(string $city, string $countryCode)
    {
        parent::__construct();

        $this->city = $city;
        $this->countryCode = $countryCode;
    }

    /**
     * build daily forecast url
     *
     * @return mixed|string
     */
    protected function buildUrl()
    {
        return $this->url .
            $this->urlSuffix .
            'city=' . $this->city .
            '&country=' . $this->countryCode .
            '&key=' . $this->key;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function getDailyAverageTemperature()
    {
        if (WeatherCache::has($this->city . '.' . $this->countryCode)) {

            return WeatherCache::get($this->city . '.' . $this->countryCode);
        }

        $response = $this->sendGet();
        $forecast = $response->getForecast();

        if (is_array($forecast)) {
            $temperature = (new CalculateAverageTemperature($forecast))->getAverageTemperature();

            WeatherCache::put($this->city . '.' . $this->countryCode, $temperature);

            return $temperature;
        }

        return false;
    }
}
