<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\ForecastRequest;
use GuzzleHttp\Exception\GuzzleException;
use App\Services\WeatherAPI\WeatherDailyRequest;

class ForecastController extends Controller {


    /**
     * return the average temperature for city
     *
     * @param ForecastRequest $request
     * @return JsonResponse
     * @throws Exception|GuzzleException
     */
    public function show(ForecastRequest $request)
    {
        $validated = $request->validated();

        $weatherAPI = new WeatherDailyRequest($validated['city'], $validated['country_code']);

        $averageTemperature = $weatherAPI->getDailyAverageTemperature();

        if ($averageTemperature) {
            return response()->json(['status' => true, 'averageTemperature' => $averageTemperature]);
        }

        return response()->json(['status' => false, 'message' => 'No Data']);
    }
}
