<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeScreenTest extends TestCase {

    /**
     * @test
     */
    public function a_user_can_see_a_home_screen()
    {
        $this->withoutExceptionHandling();

        $response = $this->get('/');

        $response->assertOk();
    }
}
