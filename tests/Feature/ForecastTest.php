<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ForecastTest extends TestCase
{
    use WithFaker;
    /**
     * @test
     */
    public function a_user_get_forecast()
    {
        $this->withoutExceptionHandling();

        $city = $this->faker->city;
        $countryCode = $this->faker->countryCode;

        $this->get("/api/forecast?city={$city}&country_code={$countryCode}")
            ->assertStatus(200)
            ->assertJsonCount(2)
            ->assertJsonStructure(['status']);
    }
}
